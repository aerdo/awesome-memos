import React from "react";

import '../Components/NavBar/NavBar';
import '../Components/MemoList/MemoList';
import NavBar from "../Components/NavBar/NavBar";
import MemoList from "../Components/MemoList/MemoList";

function Main(){
    return(
        <>
            <NavBar/>
            <MemoList/>
        </>
    )
}

export default Main;