import React, { useContext, createContext } from "react";

import Main from "./Main";
import Auth from "./Auth";

import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import { userLoggedIn, userLoggedOut } from "../Components/Store/Actions";
import { useSelector, useDispatch } from "react-redux";

const fakeAuth = {
    isAuthenticated: false,
    signIn(cb) {
        fakeAuth.isAuthenticated = true;
        setTimeout(cb, 100); // fake async
    },
    signOut(cb) {
        fakeAuth.isAuthenticated = false;
        setTimeout(cb, 100);
    }
};

const authContext = createContext();

function ProvideAuth({ children }) {
    const auth = useProvideAuth();
    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
}

export function useAuth() {
    return useContext(authContext);
}

function useProvideAuth() {
    const dispatch = useDispatch();
    const handleLoggedIn=()=>{
        dispatch(userLoggedIn())
    }
    const handleLoggedOut=()=>{
        dispatch(userLoggedOut())
    }

    const signIn = cb => {
        return fakeAuth.signIn(() => {
            setTimeout(()=>handleLoggedIn(true), 300);
            cb();
        });
    };

    const signOut = cb => {
        return fakeAuth.signOut(() => {
            setTimeout(()=>handleLoggedOut(), 300);
            cb();
        });
    };

    return {
        signIn,
        signOut
    };
}

function RedirectTo({ children, ...rest }) {
    const user = useSelector(state=>state.user);
    return (
        <Route
            {...rest}
            render={({ location }) =>
                user ? ( children ) :
                    (<Redirect
                        to={{
                            pathname: "/login",
                            state: { from: location }
                        }}
                    />)
            }
        />
    );
}

function RedirectFrom({ children, ...rest }) {
    const user = useSelector(state=>state.user);
    return (
        <Route
            {...rest}
            render={({ location }) =>
                !user ? ( children ) :
                    (<Redirect
                        to={{
                            pathname: "/",
                            state: { from: location }
                        }}
                    />)
            }
        />
    );
}

function Routing(){
    return(
        <ProvideAuth>
            <Router>
                <Switch>
                    <RedirectTo exact path="/">
                        <Main/>
                    </RedirectTo>
                    <RedirectTo exact path="/profile">
                    </RedirectTo>
                    <RedirectFrom exact path="/login">
                        <Auth/>
                    </RedirectFrom>
                </Switch>
            </Router>
        </ProvideAuth>
    )
}
export default Routing;