import React from "react";
import './Login.css'
import { useHistory, useLocation } from "react-router-dom";
import { useAuth} from "../../Pages/Routing";

function Login(){
    let history = useHistory();
    let location = useLocation();
    let auth = useAuth();

    let { from } = location.state || { from: { pathname: "/" } };
    let login = () => {
        auth.signIn(() => {
            history.replace(from);
        });
    };

    //const joke = "Те самые заметки, которые ты открываешь раз в год и недоумеваешь, в каком состоянии ты их писал";
    const joke="";

    return(
        <div className="login-container">
            <p className="login-joke" >{joke}</p>
            <div>
                <input className="login-input" placeholder="Логин"/>
            </div>
            <div>
                <input className="login-input" placeholder="Пароль" type="password"/>
            </div>
            <div>
                <button className="login-button" onClick={login}>Войти</button>
            </div>
            <div>
                <button className="login-register">Зарегистрироватся</button>
            </div>
        </div>
    )
}

export default Login;