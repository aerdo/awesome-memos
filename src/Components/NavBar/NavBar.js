import React from "react";
import './NavBar.css';

import { NavLink } from "react-router-dom";

import { changeColor } from "../Store/Actions";
import { useDispatch } from "react-redux";

import AddMemo from "./AddMemo/AddMemo";

import {useHistory} from "react-router-dom";
import {useAuth} from "../../Pages/Routing";

function NavBar(){
    const dispatch = useDispatch();
    const handleChangeColor=(color)=>{
        dispatch(changeColor(color))
    }

    let history = useHistory();
    let auth = useAuth();
    let signOut = () => {
        auth.signOut(() => history.push("/"));
    }

    /*
    *  */
    /* <div style={{textAlign: "right"}}>

                </div>

                <div className="color-mark"  style={{backgroundColor: "#F5F5F5"}} title="Профиль"
                             onClick={()=>signOut()}/>
       <li className="navbar-item" style={{display: ""}}>

                    </li>*/
    return(
        <>
            <nav className="navbar-items">
                <div className="navbar-profile">
                    <NavLink exact to ="/profile">
                        <button title="Мой профиль"><i className="far fa-user"/></button>
                    </NavLink>
                    <div style={{marginLeft: "15px"}}>Пользователь</div>
                </div>

                <ul className="navbar-menu">
                    <li className="navbar-item">
                        <AddMemo/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark multicolor" title="Отобразить всё"
                             onClick={()=>handleChangeColor("all")} style={{backgroundColor: "#F5F5F5"}}/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark white" title="Отобразить белые"
                             onClick={()=>handleChangeColor("white")}/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark pink" title="Отобразить розовые"
                             onClick={()=>handleChangeColor("pink")}/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark green" title="Отобразить зеленые"
                             onClick={()=>handleChangeColor("green")}/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark blue" title="Отобразить синие"
                             onClick={()=>handleChangeColor("blue")}/>
                    </li>
                    <li className="navbar-item">
                        <div className="color-mark yellow" title="Отобразить жёлтые"
                             onClick={()=>handleChangeColor("yellow")}/>
                    </li>
                </ul>
            </nav>

        </>
    )
}

export default NavBar;