import React, {useState} from "react";
import './AddMemo.css';

import ReactModal from 'react-modal';

function AddMemo(){
    let noScroll = require('no-scroll');
    const [open, setOpen] = useState(false);

    const acceptedTitleLength = 80;
    const acceptedTextLength = 1550;
    const [title, setTitle] = useState("");
    const [text, setText] = useState("");
    const [color, setColor] = useState("white");

    const close = () => {
        setOpen(false);
        noScroll.off();
        setText("");
        setColor("white");
    }

    const checkSize = () => {
        /* s - ~270
        * m - ~440
        * //l - ~600
        *
        * small - ~200
        * medium - ~310
        * //large - ~510 */

        const len = text.length;
        if (title.length === 0){
            // если без заголовка
            if (len <= 270){
                return "small";
            }else if (len > 440){
                return "large";
            }else{
                return "medium";
            }
        }else{
            // если без заголовка
            if (len <= 200){
                return "small";
            }else if (len > 310){
                return "large";
            }else{
                return "medium";
            }
        }
    }

    const addMemo = () => {
        const newMemo = {
            title: title,
            text: text,
            color: color,
            size: checkSize()
        }
        console.log(newMemo);
        //пуш заметки
    }

    const handleOpen=()=>{
        setOpen(true);
        noScroll.on();
    }
    const handleClose=()=>{
        close();
    }

    const handleChangeTitle = (e) => {
        if (e.target.value.length <= acceptedTitleLength){
            setTitle(e.target.value)
        }
    }

    const handleChangeText = (e) => {
        if (e.target.value.length <= acceptedTextLength){
            setText(e.target.value)
        }
    }

    const handleAdd=()=>{
        if (text.length !== 0){
            addMemo();
            close();
        }
    }

    return(
        <>
            <button title="Добавить заметку" disabled={open} className="new-note" onClick={handleOpen}>
                <div className="new-note-text">+</div>
            </button>

            <ReactModal closeTimeoutMS={150} isOpen={open} className={`Modal ${color}`} onRequestClose={handleClose}
                        overlayClassName="Overlay transparent-overlay" contentLabel="AddMemo" ariaHideApp={false}>
                <div className="new-note-panel">
                    <div className="memo-edit-title">
                        <input type="text" placeholder="Заголовок" value={title} onChange={(e)=>handleChangeTitle(e)} className={`${color}`}/>
                    </div>
                    <div className="color-container">
                        <div title="Сделать белой" className="new-memo-color white" onClick={()=>setColor("white")}/>
                        <div title="Сделать розовой" className="new-memo-color pink" onClick={()=>setColor("pink")}/>
                        <div title="Сделать зелёной" className="new-memo-color green" onClick={()=>setColor("green")}/>
                        <div title="Сделать голубой" className="new-memo-color blue" onClick={()=>setColor("blue")}/>
                        <div title="Сделать жёлтой" className="new-memo-color yellow" onClick={()=>setColor("yellow")}/>
                    </div>
                    <div className="memo-edit-body">
                        <textarea className={`${color}`} style={{height: "300px", width: "100%"}} placeholder="Текст" value={text} onChange={(e)=>handleChangeText(e)}/>
                    </div>
                    <div className="new-mem-text-len-counter">{`${text.length}/${acceptedTextLength}`}</div>
                    <div className="new-memo-actions">
                        <button onClick={handleClose} className={`new-memo-button ${color}`}>Отмена</button>
                        <button disabled={text.length === 0}  onClick={handleAdd} className={`new-memo-button ${color}`}>Добавить</button>
                    </div>
                </div>
            </ReactModal>
        </>
    )
}

export default AddMemo;