import React from "react";
import {useSelector} from "react-redux";

import Card from "./Card/Card";

function MemoList(){
    const allMemos = useSelector((state) => state.memoList);

    const color = useSelector((state)=>state.label);
    let memos;
    if (color === "all"){
        memos = allMemos
    }else{
        memos = allMemos.filter(item => item.color === color);
    }

    return(
        <div style={styles.page}>
            <div style={styles.memoContainer}>
                {memos.map((item,id)=>{
                    return(
                        <Card key={id} item={item}/>
                    )
                })}
            </div>
        </div>

    )
}

const styles = {
    page: {
        marginRight: "calc(-1 * (100vw - 100%))",
        marginTop: '20px',
        marginBottom: '40px',
        display: 'flex',
        justifyContent: 'center'
    },
    memoContainer: {
        margin: 0,
        padding: 0,
        width: '90vw',
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, 340px)',
        gridAutoRows: '10px'
    }
}


export default MemoList;