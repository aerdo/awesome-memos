export const MEMOS=[
    {
        id: 1,
        title: "Предназначение 1",
        text: "Сделать при нажатии на Enter сохранение/добавление закладки\nА на Esc отмену-назад",
        color: "pink",
        size: "small"
    },
    {
        id: 2,
        title: "Предназначение 2",
        text: "сделать кастомные папки",
        color: "pink",
        size: "small"
    },
    {
        id: 3,
        title: "",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "yellow",
        size: "small"
    },
    {
        id: 4,
        title: "",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "blue",
        size: "medium"
    },
    {
        id: 5,
        title: "",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "green",
        size: "large"
    },{
        id: 6,
        title: "Заметка 1",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "yellow",
        size: "small"
    },
    {
        id: 7,
        title: "Заметка 2",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "blue",
        size: "medium"
    },
    {
        id: 8,
        title: "Заметка 3",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Libero id faucibus nisl tincidunt. Turpis egestas sed tempus urna et pharetra. Ultricies mi eget mauris pharetra et ultrices. Nisl tincidunt eget nullam non nisi. Rhoncus mattis rhoncus urna neque viverra. Sed id semper risus in hendrerit. Laoreet suspendisse interdum consectetur libero id faucibus nisl tincidunt. Pulvinar sapien et ligula ullamcorper malesuada proin libero nunc. Adipiscing tristique risus nec feugiat in fermentum posuere urna nec. Aliquam etiam erat velit scelerisque in dictum. Eget nunc scelerisque viverra mauris in aliquam. Odio ut sem nulla pharetra diam sit amet nisl suscipit. Vitae turpis massa sed elementum tempus egestas sed. Arcu cursus euismod quis viverra nibh.",
        color: "green",
        size: "large"
    },
    {
        id: 9,
        title: "нужная заметка",
        text: "мне нужна эта заметка для того, чтобы посмотреть на то, как отображается контент",
        color: "white",
        size: "small"
    },
    {
        id: 10,
        title: "СРОЧНО!!!",
        text: "помой посуду :((",
        color: "green",
        size: "small"
    }
]

