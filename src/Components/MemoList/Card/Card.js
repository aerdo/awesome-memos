import React, {useState} from "react";
import './Card.css'

import ReactModal from 'react-modal';

function Card(props){
    const memo=props.item;
    let noScroll = require('no-scroll');
    const [open, setOpen] = useState(false);
    const [save, setSave] = useState(false);

    const acceptedTitleLength = 80;
    const acceptedTextLength = 1550;
    const [title, setTitle] = useState(memo.title);
    const [text, setText] = useState(memo.text);
    const [color, setColor] = useState(memo.color);


    const close = () => {
        setOpen(false);
        setSave(false);
        noScroll.off();
    }

    const checkSize = () => {
        const len = text.length;
        if (title.length === 0){
            // если без заголовка
            if (len <= 270){
                return "small";
            }else if (len > 440){
                return "large";
            }else{
                return "medium";
            }
        }else{
            // если без заголовка
            if (len <= 200){
                return "small";
            }else if (len > 310){
                return "large";
            }else{
                return "medium";
            }
        }
    }

    const addMemo = () => {
        /*const newMemo = {
            title: title,
            text: text,
            color: color,
            size: checkSize()
        }*/
        //console.log(newMemo);

        //пуш заметки
    }

    const handleOpen=()=>{
        setOpen(true);
        noScroll.on();

        setColor(memo.color);
        setTitle(memo.title);
        setText(memo.text);
    }

    const handleClose=()=>{
        close();
    }

    const handleDiscard = () => {
        // перемещение заметки в папку корзина
        close();
    }

    const handleChangeTitle = (e) => {
        if (e.target.value.length <= acceptedTitleLength){
            setTitle(e.target.value);
            setSave(true);
        }
    }

    const handleChangeText = (e) => {
        if (e.target.value.length <= acceptedTextLength){
            setText(e.target.value);
            setSave(true);
        }
    }

    const handleAdd=()=>{
        if (text.length !== 0){
            addMemo();
            close();
        }
    }

    return(
        <>
            <div style={{...styles.card, ...styles[memo.size]}} className={`memo ${memo.color}`} onClick={handleOpen}>
                <div style={{ padding: "0 25px 0 25px", textAlign: "center", marginTop: "15px", marginBottom: "15px"}}>
                    <div style={memo.title ? {paddingBottom: "10px"} : {display: "none"}} className="memo-title">
                        {memo.title}
                    </div>

                </div>
                <div style={{padding: "0 25px 0 25px", wordBreak: "break-word", display: "-webkit-box", overflow: "hidden",
                    fontFamily: "Roboto, sans-serif", fontStyle: "normal", fontWeight: "300", fontSize: "16px"}}
                     className={`trunk-lines ${memo.title ? sizes[memo.size] : sizesTitle[memo.size]}`} >
                    {memo.text}
                </div>
            </div>

            <ReactModal closeTimeoutMS={150} isOpen={open} className={`Modal ${color}`} onRequestClose={handleClose}
                        overlayClassName="Overlay transparent-overlay" contentLabel="AddMemo" ariaHideApp={false}>
                <div className="new-note-panel">
                    <div className="memo-edit-title">
                        <input title="Может быть пустым :)" type="text" placeholder="Заголовок" value={title} onChange={(e)=>handleChangeTitle(e)} className={`${color}`}/>
                    </div>
                    <div className="color-container">
                        <div title="Сделать белой" className="new-memo-color white" onClick={()=>setColor("white")}/>
                        <div title="Сделать розовой" className="new-memo-color pink" onClick={()=>setColor("pink")}/>
                        <div title="Сделать зелёной" className="new-memo-color green" onClick={()=>setColor("green")}/>
                        <div title="Сделать голубой" className="new-memo-color blue" onClick={()=>setColor("blue")}/>
                        <div title="Сделать жёлтой" className="new-memo-color yellow" onClick={()=>setColor("yellow")}/>
                    </div>
                    <div className="memo-edit-body">
                        <textarea className={`${color}`} style={{height: "300px", width: "100%"}} placeholder="Текст"
                                  value={text} onChange={(e)=>handleChangeText(e)}/>
                    </div>
                    <div className="new-mem-text-len-counter">{`${text.length}/${acceptedTextLength}`}</div>
                    <div className="new-memo-actions">
                        <button title="Переместить в корзину" onClick={handleDiscard} className={`new-memo-button ${color}`}>
                            <i className="fas fa-trash-alt"/>
                        </button>
                        <button onClick={handleClose} className={`new-memo-button ${color}`}>Назад</button>
                    </div>

                </div>
            </ReactModal>
        </>
    )
}

const styles = {
    card: {
        margin: '15px',
        padding: 0,
        borderRadius: '10px',
        border: '1px solid black',
    },
    small: {
        gridRowEnd: 'span 23'
    },
    medium: {
        gridRowEnd: 'span 30'
    },
    large: {
        gridRowEnd: 'span 41'
    }
}

const sizes = {
    small: "trunc-lines-small",
    medium: "trunc-lines-medium",
    large: "trunc-lines-large"
}

const sizesTitle = {
    small: "trunc-lines-s",
    medium: "trunc-lines-m",
    large: "trunc-lines-l"
}

export default Card;