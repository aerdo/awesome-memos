const initialColor = localStorage.getItem('color') ? localStorage.getItem('color') : "all";

const initialState = {
    label: initialColor,
    user: localStorage.getItem('user'),
    memoList: JSON.parse(localStorage.getItem('memoList'))
}

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case "SHOW_COLOR":
            return {...state, label: action.label};
        case "USER_LOGGED_IN":
            return {...state, user: true, memoList: action.memoList};
        case "USER_LOGGED_OUT":
            return {...state, user: false};
        case 'GET_MEMO_LIST':
            return {...state, memoList: action.memoList}
        case 'SET_MEMO_LIST':
            return {...state, memoList: action.memoList}
        default:
            return state;
    }
}
