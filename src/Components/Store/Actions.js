import {MEMOS} from "../MemoList/Memos";

export const changeColor = (color) => {
    localStorage.setItem('color', color);
    return {
        type: `SHOW_COLOR`,
        label: color
    };
};

export const userLoggedIn = () => {
    localStorage.setItem('user', 'id');
    const list = MEMOS;
    localStorage.setItem('memoList', JSON.stringify(list));
    return{
        type: 'USER_LOGGED_IN',
        memoList: list
    };
}

export const userLoggedOut = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('memoList');
    return{
        type: 'USER_LOGGED_OUT'
    };
}

export const setMemoList = (list) => {
    localStorage.setItem('memoList', JSON.stringify(list));
    return{
        type: 'SET_MEMO_LIST'
    }
}

export const getMemoList = () => {
    const list = JSON.parse(localStorage.getItem('memoList'))
    return {
        type: 'GET_MEMO_LIST',
        memoList: list
    }
}
